import React from "react";
import { v4 } from "uuid";
import List from "@material-ui/core/List";
//import { unstable_Box as Box } from '@material-ui/core/Box';

import ListAdd from "./listAdd";
import ListItem from "./listItem";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
const flexContainer = {
	display: "flex",
	flexWrap: "wrap"
};
class ListOfCards extends React.Component {
	constructor(props) {
		super(props);
		this.addNewCard = this.addNewCard.bind(this);
		this.handleClickOpen = this.handleClickOpen.bind(this);
		this.editCard = this.editCard.bind(this);
    this.deleteCard = this.deleteCard.bind(this);
    this.editContentOfTheCard=this.editContentOfTheCard.bind(this);
		this.state = {
			open: false,
			ids: [],
			byIds: {},
			titles: [],
			title: "",
			editId: null
		};
	}
	addNewCard() {
		
			let id = v4();
			this.setState({
				ids: [...this.state.ids, id],
				byIds: {
					...this.state.byIds,
					[id]: {
						title: this.state.title,
						_id: id
					}
				}
			});
		this.handleClose();
	}
	handleClickOpen = () => {
		this.setState({ open: true });
	};
	handleChange = name => event => {
		this.setState({ [name]: event.target.value });
	};
	handleClose = () => {
		this.setState({ 
      open: false,
      title:''
     });
	};
	deleteCard(delId) {
		let tempByIds = { ...this.state.byIds };
		var idArray = [...this.state.ids]; // make a separate copy of the array
		var index = idArray.indexOf(delId);
		if (index !== -1) {
      idArray.splice(index, 1);
      delete tempByIds[delId];
			this.setState({ 
        ids: idArray,
        byIds: { ...tempByIds }
      });
		}
	}
	editCard(id) {
		this.setState({
      editId: id,
      title:this.state.byIds[id].title
		});
		this.handleClickOpen();
	}
	editContentOfTheCard() {
		this.setState({
			byIds: {
				...this.state.byIds,
				[this.state.editId]: {
					title: this.state.title,
					_id: this.state.editId
				}
			}
    });
    this.handleClose();
	}
	render() {
		return (
			<>
				<List style={flexContainer}>
					<ListAdd
						handleClickOpen={this.handleClickOpen}
						addNewCard={this.addNewCard}
					/>
					{this.state.ids.map(id => (
						<ListItem
							handleDelete={this.deleteCard}
							style={{ padding: 15, margin: 20 }}
							key={id}
							id={id}
							handleEdit={this.editCard}
							title={this.state.byIds[id].title}
						/>
					))}
				</List>
				<Dialog
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="form-dialog-title"
				>
					{/* <DialogTitle id="form-dialog-title">Add NewCard</DialogTitle> */}
					<DialogContent>
						{/* <DialogContentText>
              Enter the title
            </DialogContentText> */}
						<TextField
							autoFocus
							margin="dense"
							id="name"
							label="Title"
							type="text"
							value={this.state.title}
							onChange={this.handleChange("title")}
							fullWidth
						/>
					</DialogContent>
					<DialogActions>
						{!this.state.editId ? (
							<Button onClick={this.addNewCard} color="primary">
								Add
							</Button>
						) : (
							<Button onClick={this.editContentOfTheCard} color="primary">
								Edit
							</Button>
						)}
						<Button onClick={this.handleClose} color="secondary">
							Cancel
						</Button>
					</DialogActions>
				</Dialog>
			</>
		);
	}
}
export default ListOfCards;
