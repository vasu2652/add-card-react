import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import red from "@material-ui/core/colors/red";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
const styles = theme => ({
	card: {
		maxWidth: 400
	},
	media: {
		height: 0,
		paddingTop: "56.25%" // 16:9
	},
	actions: {
		display: "flex"
	},
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest
		})
	},
	expandOpen: {
		transform: "rotate(180deg)"
	},
	avatar: {
		backgroundColor: red[500]
	}
});
class ListAdd extends React.Component {
	state = {
		expanded: false,
  };
  handleClickOpen=()=>{
    this.props.handleClickOpen();
  }
	render() {
		const { classes } = this.props;
		return (
			<>
				<Card
					className={classes.card}
					style={{ flexBasis: "18%", margin: "2%" }}
				>
					<CardContent>
          <CardHeader
          // avatar={
          //   <Avatar aria-label="Recipe" className={classes.avatar}>
          //     R
          //   </Avatar>
          // }
          
          // title="Shrimp and Chorizo Paella"
          // subheader="September 14, 2016"
        />
						<Typography gutterBottom variant="h5" component="h2">
							<Fab
								color="primary"
								aria-label="Add"
								className={classes.fab}
								onClick={this.handleClickOpen}
							>
								<AddIcon />
							</Fab>
						</Typography>
					</CardContent>
				</Card>
				
			</>
		);
	}
}

ListAdd.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListAdd);
