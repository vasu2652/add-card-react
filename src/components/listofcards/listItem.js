import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});
class ListItem extends React.Component {
  state = { 
    expanded: false,
    anchorEl: null
   };

  openMenu=(e)=>{
    this.setState({
      anchorEl: e.currentTarget
    });
  }
  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleEdit=(id)=>()=>{
    this.props.handleEdit(id);
  }
  handleDelete=(id)=>()=>{
    this.props.handleDelete(id);
  }
  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    return (
      <>
      <Card style={{flexBasis:"18%", margin:"2%"}} className={classes.card}>
        <CardHeader
          // avatar={
          //   <Avatar aria-label="Recipe" className={classes.avatar}>
          //     R
          //   </Avatar>
          // }
          action={
            <IconButton 
            aria-owns={anchorEl ? 'simple-menu' : undefined}
            aria-haspopup="true"
            onClick={this.openMenu}>
              <MoreVertIcon />
            </IconButton>
          }
          // title="Shrimp and Chorizo Paella"
          // subheader="September 14, 2016"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {this.props.title}
          </Typography>
        </CardContent>

      </Card>
      <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleEdit(this.props.id)}>Edit</MenuItem>
          <MenuItem onClick={this.handleDelete(this.props.id)}>Delete</MenuItem>
        </Menu>
      
      </>
    );
  }
}

ListItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListItem);