import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Boolean from './Boolean';
const styles = theme => ({
	container: {
	  display: 'flex',
	  flexWrap: 'wrap',
	},
	textField: {
	  marginLeft: theme.spacing.unit,
	  marginRight: theme.spacing.unit,
	  width: '100%',
	},
	dense: {
	  marginTop: 19,
	},
	menu: {
	  width: 200,
	},
  });

class TextArea extends React.Component {
  constructor(props){
    super(props);
    this.handleChange=this.handleChange.bind(this);
  }
  state = {
    multiline: '',
  };
  handleChange = (name,onClick) => event => {
	onClick({value: event.target.value})
    this.setState({ [name]: event.target.value });
  };
  render() {
    const { classes, onClick, label } = this.props;

    return (
      <div className={classes.root} style={{ width: '100%' }}>
        <TextField
          id="standard-multiline-static"
          label={label}
          multiline
          rows="4"
          value={this.state.multiline}
          onChange={this.handleChange('multiline',onClick)}
          className={classes.textField}
          margin="normal"
		  variant="outlined"
        />
		
      </div>
    );
  }
}

TextArea.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextArea);

