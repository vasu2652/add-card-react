import { VariableSizeList as List } from 'react-window';
import React, { forwardRef } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'
import fill from 'lodash/fill';
const styles = theme => ({
	root: {
		width: '100%',
		marginTop: theme.spacing.unit * 3,
		overflowX: 'auto',
	},
	table: {
		minWidth: 700,
	},
	row: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.background.default,
		},
	},
});
const CustomTableCell = withStyles(theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);
const tableHeaders = ['Conversations', 'compliance__', 'ngrams_freqs', 'trascription', 'callwordfreqs', 'classification', 'caas template'];

const tableData = [
	['Frozen yoghurt', 159, 6.0, 24, 4.0],
	['Ice cream sandwich', 237, 9.0, 37, 4.3],
	['Eclair', 262, 16.0, 24, 6.0],
	['Cupcake', 305, 3.7, 67, 4.3],
	['Gingerbread', 356, 16.0, 49, 3.9]
];
const listRef = React.createRef();
class ComponentThatRendersAListOfItems extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			rowHeights: fill(new Array(50), 100),
			itemSize: 50,
			height: 300
		}
		this.changeItemSize = this.changeItemSize.bind(this);
		this.getItemSize = this.getItemSize.bind(this);
	}
	getItemSize(index) {
		console.log('======', this.state.rowHeights[index]);
		return this.state.rowHeights[index];
	}
	changeItemSize(index) {
		const newState = this.props.changeItemSize(index, this.state, this.setState, this.listRef);
		if (newState.update) {
			this.setState(() => ({
				rowHeights: newState.rowHeights,
			}), () => {
				this.listRef.resetAfterIndex(index, true)
			})
		}
	}
	render() {
		console.log(this.state.rowHeights);
		// Pass items array to the item renderer component as itemData
		return (
			<List
				itemData={{ 'data': fill(new Array(50), 2), 'func': this.changeItemSize }}
				height={this.state.height}
				width={"100%"}
				itemCount={1}
				itemSize={this.getItemSize}
				ref={ref => { this.listRef = ref }}
			>
				{withStyles(styles)(ItemRenderer)}
			</List>
		);
	}
}
class ItemRenderer extends React.PureComponent {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}
	handleClick() {
		this.props.data.func(this.props.index);
	}
	componentWillReceiveProps() {
		console.log("--vas", this.props);
	}
	render() {
		var { index, style, classes }=this.props;
		console.log("...",style);
		const { data } = { ...this.props.data };
		const backgroundColor = this.props.index % 2 ? 'red' : 'white';
		return (
			// <div onClick={this.handleClick} style={{...this.props.style, backgroundColor}}>
			// 	{data[this.props.index]}
			// </div>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						{tableHeaders.map(head => <CustomTableCell align="center">{head}</CustomTableCell>)}
					</TableRow>
				</TableHead>
				<TableBody>
					{tableData.map((row, index) =>
						<TableRow style={{...style,position:"relative"}} className={classes.row}  key={index} >
							{row.map((item, i) =>
								<CustomTableCell onClick={this.handleClick} align="center" key={i} component="th" scope="row">
									{item}
								</CustomTableCell>
							)}
						</TableRow>
					)}
				</TableBody>
			</Table>
		)
	}
}


export default () => {
	const setItemSize = (index, state) => {
		const newHeights = [].concat(state.rowHeights)
		if (state.rowHeights[index] === 100) {
			newHeights[index] = state.rowHeights[index] + 100;
		} else {
			newHeights[index] = state.rowHeights[index] - 100;
		}
		return {
			rowHeights: newHeights,
			update: true,
		}
	}
	return <ComponentThatRendersAListOfItems changeItemSize={setItemSize} />
}