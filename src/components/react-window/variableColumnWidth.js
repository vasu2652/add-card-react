import React from 'react';
import { VariableSizeList as List } from 'react-window';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

let id = 0;

function createData(name, calories, fat, carbs, protein) {
	id += 1;
	return { id, name, calories, fat, carbs, protein };
}

// function createData1(tableHeaders,tableData) {
// 	id += 1;
//   return { id, name, calories, fat, carbs, protein };
// }
const tableHeaders = ['Conversations', 'compliance__', 'ngrams_freqs', 'trascription', 'callwordfreqs', 'classification','caas template'];
const tableData = [
	['Frozen yoghurt', 159, 6.0, 24, 4.0],
	['Ice cream sandwich', 237, 9.0, 37, 4.3],
	['Eclair', 262, 16.0, 24, 6.0],
	['Cupcake', 305, 3.7, 67, 4.3],
	['Gingerbread', 356, 16.0, 49, 3.9]
];
// const rows = [
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];
const CustomTableCell = withStyles(theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);
// These column widths are arbitrary.
// Yours should be based on the content of the column.
const columnWidths = new Array(10)
	.fill(true)
	.map(() => 75 + Math.round(Math.random() * 10));

const getItemSize = (index) => {

	return columnWidths[index];
}
const styles = theme => ({
	root: {
		width: '100%',
		marginTop: theme.spacing.unit * 3,
		overflowX: 'auto',
	},
	table: {
		minWidth: 700,
	},
	row: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.background.default,
		},
	},
});
const Column = ({ index, style, classes }) => (
	<Table className={classes.table}>
		<TableHead>
			<TableRow>
				{tableHeaders.map(head => <CustomTableCell align="center">{head}</CustomTableCell>)}
			</TableRow>
		</TableHead>
		<TableBody>
			{tableData.map((row, index) =>
				<TableRow className={classes.row} key={index}>
					{row.map((item, i) =>
						<CustomTableCell align="center" key={i} component="th" scope="row">
							{item}
						</CustomTableCell>
					)}
				</TableRow>
			)}
		</TableBody>
	</Table>
);
export const VariableColumn = () => (
	<List
		height={250}
		itemCount={1}
		itemSize={getItemSize}
		// layout="horizontal"
		width={700}
	>
		{withStyles(styles)(Column)}
	</List>
);