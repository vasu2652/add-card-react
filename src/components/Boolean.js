import React from 'react';
import url from 'url';
import PropTypes from 'prop-types';
import { withStyles, withTheme, createMuiTheme } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const styles = theme => ({
  root: {
    // padding: theme.spacing.unit * 2,
    float:'right',
    paddingTop: 0,
    //border: '1px solid #f2f2f2',
    // Match <Inspector /> default theme.
    backgroundColor: theme.palette.type === 'light' ? theme.palette.common.white : '#242424',
    //minHeight: theme.spacing.unit * 40,
    // width: '100%',
    // margin: 10,
    width: '100%',
    left: 10,
    position: 'relative'

  },
  switch: {
    paddingBottom: theme.spacing.unit,
  },
});

class BooleanSwitch extends React.Component {
  state = {
    checked: false,
  };

  componentDidMount() {
    const URL = url.parse(document.location.href, true);
    const expandPath = URL.query['expend-path'];
    this.setState({
      checked: this.props.value
    })
    if (!expandPath) {
      return;
    }
  }

  render() {
    const { classes, value, onClick,label, style } = this.props;
    const { checked } = this.state;

    return (
      <div className={classes.root} style={style}>
        <FormControlLabel
          className={classes.switch}
          control={
            <Switch
              checked={checked}
              onChange={(event, value) => {
                onClick({ value })
                this.setState({ checked: value })
              }}
            />
          }
          label={label}
                  />
      </div>
    );
  }
}

BooleanSwitch.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(withTheme()(BooleanSwitch));
