import React from 'react';
import { DateRangePicker,  } from 'react-dates';

class Date extends React.Component {
    state = {
        startDate: null,
        endDate:null,
        focusedInput: null
    };

    render() {
        const { classes,onClick } = this.props;

        return (
            <DateRangePicker
                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                onDatesChange={({ startDate, endDate }) => {
                    let value=`${startDate && startDate._d?startDate._d:''} % ${endDate && endDate._d?endDate._d:''}`
                    onClick({
                        value
                    })
                    this.setState({ startDate, endDate })}
                } // PropTypes.func.isRequired,
                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
            />
        );
    }
}
export default Date;
