import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  SimpleSelect  from '../components/selectInput';

const SelectControl = ({ data, handleChange, path,label }) => {
  //console.log(data);
  return <SimpleSelect
  value={data}
  label={label}
  onClick={ev => handleChange(path, (ev.value))}
/>
}
  

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(SelectControl);