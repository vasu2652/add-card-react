import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  Division  from '../components/Division';
const DivisionControl = (props) => {
  console.log(props)
	return <Division
    label={props.label}
    style={props.uischema.style}
  />
};

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(DivisionControl);