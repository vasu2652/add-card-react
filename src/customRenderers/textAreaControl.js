import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  TextArea  from '../components/textArea';

const TextAreaControl = ({ data, handleChange, path, label }) => {
	// console.log(label);
	return <TextArea
	value={data}
	label={label}
    onClick={ev => handleChange(path, (ev.value))}
  />
}
  

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(TextAreaControl);