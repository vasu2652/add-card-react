import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  Date  from '../components/Date';

const DateControl = ({ data, handleChange, path,label }) => {
  console.log("date",data);
  return <Date
    value={data}
    label={label}
    onClick={ev => {
      console.log(ev);
      return handleChange(path, ev.value)}
    }
  />
}
  


export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(DateControl);