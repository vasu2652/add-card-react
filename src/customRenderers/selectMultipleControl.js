import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import MultipleSelect  from '../components/selectMultiple';

const SelectMultipleControl = ({ data, handleChange, path,label }) => {
	console.log(data);
	return <MultipleSelect
    value={data}
    label={label}
    onClick={ev => handleChange(path, Number(ev.value))}
  />
}
  

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(SelectMultipleControl);