import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import ComponentThatRendersAListOfItems from './components/react-window/dynamicTableActions';
import { VariableColumn } from './components/react-window/variableColumnWidth';
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { Actions, jsonformsReducer } from '@jsonforms/core';
import { materialFields, materialRenderers } from '@jsonforms/material-renderers';
//import './index.css';
//import App from './App';
//import * as serviceWorker from './serviceWorker';
import BooleanControlTester from './customTesters/BooleanControlTester';
import DivisionControlTester from './customTesters/DivisionControlTester';
import BooleanControl from './customRenderers/BooleanControl';
import SearchControlTester from './customTesters/searchControlTester';
import SearchControl from './customRenderers/searchControl';
import dateControl from './customRenderers/dateControl';
import dateControlTester from './customTesters/dateControlTester';
import TextAreaControlTester from './customTesters/textAreaControlTester';
import TextAreaControl from './customRenderers/textAreaControl';
import DivisionControl from './customRenderers/divisionControl';
import TextInputControlTester from './customTesters/textControlTester';
import TextInputControl from './customRenderers/textControl';
import SelectControl from './customRenderers/selectControl';
import SelectControlTester from './customTesters/selectControlTester';
import SelectMultipleControlTester from './customTesters/selectMultipleControlTester';
import SelectMultipleControl from './customRenderers/selectMultipleControl';
// import uischema from './uischema.json';
// import schema from './jsonschema.json';
import uischema from './caasUiSchema.json';
import uiSchemaFunc from './functions/uiSchema';
import schema from './caasJsonSchema.json';
// var uischema=uiSchemaFunc.getFormComponent(0);
// console.log(uischema);
var data = {

};
const store = createStore(
	combineReducers({ jsonforms: jsonformsReducer() }),
	{
		jsonforms: {
			renderers: materialRenderers,
			fields: materialFields,
		}
	}
);
store.dispatch(Actions.init(data, schema, uischema));
// store.dispatch(Actions.registerRenderer(SearchControlTester, SearchControl));
// store.dispatch(Actions.registerRenderer(SelectControlTester, SelectControl));
store.dispatch(Actions.registerRenderer(BooleanControlTester, BooleanControl));
// store.dispatch(Actions.registerRenderer(dateControlTester, dateControl));
store.dispatch(Actions.registerRenderer(TextAreaControlTester, TextAreaControl));
store.dispatch(Actions.registerRenderer(DivisionControlTester, DivisionControl));
store.dispatch(Actions.registerRenderer(TextInputControlTester, TextInputControl));
// store.dispatch(Actions.registerRenderer(SelectMultipleControlTester, SelectMultipleControl));
ReactDOM.render(
	<Provider>
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={App} />
				{/* both /roster and /roster/:number begin with /roster */}
				<Route exact path="/column" component={VariableColumn} />
				<Route exact path="/dta" component={ComponentThatRendersAListOfItems} />
			</Switch>
		</BrowserRouter>
	</Provider>
	,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
