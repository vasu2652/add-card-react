import React, { Component } from 'react';
import './App.css';
import ListOfCards from './components/listofcards/index';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ListOfCards/>
      </div>
    );
  }
}

export default App;
